# The Coder badge.

The button kicks off a workspace build, based on the spec defined in `.coder/react.yml`.

Coder pulls in the template through an OAuth integration with GitLab.

[![Open in Coder](https://demo-2.cdr.dev/static/image/embed-button.svg)](https://demo-2.cdr.dev/wac/build?template_oauth_service=63542440-047134eb3cef33046b8e63ab&template_url=https://gitlab.com/ericpaulsen/workspace-templates&template_ref=master&template_filepath=.coder/react.yml)
